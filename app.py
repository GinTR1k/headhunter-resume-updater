import os
import sys
import logging
from dotenv import load_dotenv
import requests
import json
from telebot import TeleBot


logger = logging.getLogger(__file__)
logger.setLevel(logging.DEBUG)

logger_handler = logging.StreamHandler()
logger_handler.setLevel(logging.DEBUG)

formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger_handler.setFormatter(formatter)

logger.addHandler(logger_handler)


basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))

telegram_bot = None
if (os.environ.get('TELEGRAM_TOKEN')
        and os.environ.get('TELEGRAM_NOTIFICATION_CHAT_ID')):
    telegram_bot = TeleBot(os.environ.get('TELEGRAM_TOKEN'))
else:
    logger.warn('Telegram token or notification char_id not found')


class HeadHunter():
    api_url = 'https://api.hh.ru'

    def __init__(self):
        self.access_token = os.environ.get('HH_ACCESS_TOKEN')
        self.refresh_token = os.environ.get('HH_REFRESH_TOKEN')
        self.headers = {
            'Authorization': 'Bearer ' + self.access_token,
            'User-Agent': 'Quick Edit/0.1 (administrator@gintr1k.space)'
        }

        if not self.access_token:
            raise ValueError('Access Token not found')

        if not self.refresh_token:
            raise ValueError('Refresh Token not found')

    def test_token(self):
        self._send_request(f'/me', 'GET')

    def short_cv_id(self, cv_id):
        return ''.join((cv_id[:6], '...', cv_id[-6:]))

    def _send_request(self, address, method='POST', api_url=None,
                      parameters=dict(), data=dict(), headers=dict()):
        logger.debug(f'Sending request: {method} {address}')

        if headers is not None:
            headers = {**self.headers, **headers}

        if api_url is None:
            api_url = self.api_url

        response = None
        if method == 'POST':
            response = requests.post(
                api_url + address, data=data, headers=headers)
        elif method == 'GET':
            response = requests.get(
                api_url + address, params=parameters,
                headers=headers)

        response_content = json.loads(
            response.content if response.content else '{}')

        logger.debug(
            f'Recived response with HTTP Status Code {response.status_code}')

        if response.status_code >= 200 and response.status_code < 300:
            return response_content

        logger.debug('Possible error: Status code not in 2xx')
        if not self._find_error(response_content):
            raise RuntimeError(
                'Got global error:\n' + str(
                    response.text if not response_content
                    else response_content)
            )

    def _find_error(self, data):
        if 'errors' in data:
            errors = ''
            for error in data['errors']:
                errors += f'Value: \"{error["value"]}\", ' + \
                    f'Type: \"{error["type"]}\"' + '\n'

            raise RuntimeError(f'HeadHunter returned errors:\n{errors}')

        return False

    def get_cv_list(self):
        logger.debug('Getting CVs list...')
        return self._send_request('/resumes/mine', 'GET')

    def update_cv(self, cv_id):
        logger.debug(f'Updating CV with id = {self.short_cv_id(cv_id)}...')

        try:
            self._send_request(f'/resumes/{cv_id}/publish', 'POST')
        except Exception:
            logger.info(
                f'CV with id = {self.short_cv_id(cv_id)} ' +
                'not updated: Possibly, the update is not available yet')
            return False

        return True

    def update_cvs(self):
        logger.info('Updating all CVs...')

        cv_list_total = 0
        cv_updated_count = 0

        cv_list = self.get_cv_list()
        for cv in cv_list['items']:
            if cv['status']['id'] == 'published':
                cv_list_total += 1

                if self.update_cv(cv['id']):
                    cv_updated_count += 1

        logger.info(f'Updating CVs done ({cv_updated_count}/{cv_list_total})')

        return cv_updated_count

    def refresh_access_token(self):
        response = self._send_request(
            '/oauth/token', method='POST', api_url='https://hh.ru',
            data={
                'grant_type': 'refresh_token',
                'refresh_token': self.refresh_token
            })
        return response


if __name__ == '__main__':
    logger.info('Program started!')

    hh = HeadHunter()

    if len(sys.argv) > 0:
        logger.debug(f'Got {len(sys.argv)} arguments: {sys.argv}')

        if 'refresh' in sys.argv:
            token_data = hh.refresh_access_token()
            print(token_data)

            if telegram_bot:
                telegram_bot.send_message(
                    os.environ.get('TELEGRAM_NOTIFICATION_CHAT_ID'),
                    '*You have successfully refreshed token!*\n\n' +
                    'New API credentials:\n' +
                    f'*ACCESS_TOKEN*: `{token_data["access_token"]}`\n' +
                    f'*REFRESH_TOKEN*: `{token_data["refresh_token"]}`\n',
                    parse_mode='markdown'
                )

            sys.exit()
        elif __file__ in sys.argv:
            pass
        else:
            raise ValueError('Unknown argument passed')

    try:
        hh.test_token()
    except Exception as error:
        logger.error(error)

        if telegram_bot:
            telegram_bot.send_message(
                os.environ.get('TELEGRAM_NOTIFICATION_CHAT_ID'),
                'You may have errors with your *ACCESS_TOKEN*!\n' +
                f'```\n{error}\n```\n' +
                'If your access token has expired, you can get new ' +
                'pair of tokens by running application with argument' +
                ' `refresh` (`python app.py refresh`).\n' +
                'Then, you need to change the `.env` file.',
                parse_mode='markdown'
            )

        sys.exit()

    hh.update_cvs()
